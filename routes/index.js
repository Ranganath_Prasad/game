
/*
 * GET home page.
 */

exports.index = function(req, res){
  res.render('index', {name:'waterfall'})
};

exports.commonIndex = function(req, res){
	var name = req.param('name',null);
  res.render('index',{name:name});
};